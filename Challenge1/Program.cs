﻿using System.Globalization;

namespace Challenge1;
class Program
{
    static void Main(string[] args)
    {
        IsFileExist();

        Repository repository = new Repository("data.txt");

        do
        {
            int workMode = ChooseMode();
            switch (workMode)
            {
                case 1:
                    repository.PrintAllWorkers();
                    break;
                case 2:
                    int idForPrint = ReadInt("Введите id сотрудника");
                    repository.PrintWorkers(repository.GetWorkerById(idForPrint));
                    break;
                case 3:
                    Console.WriteLine("Введите Ф.И.О сотрудника");
                    string fullName = Console.ReadLine()!;
                    int age = ReadInt("Введите возраст сотрудника");
                    int height = ReadInt("Введите рост сотрудника");
                    DateTime dateOfBirth = ReadDateTime("Введите дату рождения сотрудника(dd.mm.yyyy)");
                    Console.WriteLine("Введите место рождения сотрудника");
                    string territory = Console.ReadLine()!;
                    repository.AddWorker(fullName, age, height, dateOfBirth, territory);
                    break;
                case 4:
                    int idForDelete = ReadInt("Введите id сотрудника");
                    repository.DeleteWorker(idForDelete);
                    break;
                case 5:
                    DateTime startDate = ReadDateTime("Введите начальную дату(dd.mm.yyyy)");
                    DateTime endDate = ReadDateTime("Введите конечную дату(dd.mm.yyyy)");
                    repository.PrintWorkers(repository.GetWorkersBetweenTwoDates(startDate, endDate));
                    break;
                default:
                    return;
            }
        } while (true);
    }

    static DateTime ReadDateTime(string message)
    {
        do
        {
            Console.WriteLine(message);
            if (DateTime.TryParseExact(Console.ReadLine()!, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime))
            {
                return dateTime;
            }
        } while (true);
    }

    static int ReadInt(string message)
    {
        do
        {
            Console.WriteLine(message);
            if (int.TryParse(Console.ReadLine()!, out int integer))
            {
                return integer;
            }
        } while (true);
    }

    static int ChooseMode()
    {
        bool isModeChoosed = false;
        int isWriteData;
        do
        {
            Console.Write("Выберите режим работы (1 - вывести данные, 2 - вывести данные одного работника, 3 - заполнить данные, 4 - удалить работника, 5 - показать работников между двумя датами, 6 - выход): ");
            isWriteData = int.Parse(Console.ReadLine()!);
            if (!(isWriteData > 0 && isWriteData < 7))
            {
                Console.WriteLine("Введите 1, 2, 3, 4, 5 или 6");
            }
            else
            {
                isModeChoosed = true;
            }
        } while (!isModeChoosed);
        return isWriteData;
    }

        static void IsFileExist()
    {
        if (!File.Exists("data.txt"))
        {
            File.Create("data.txt");
        }
    }
}

