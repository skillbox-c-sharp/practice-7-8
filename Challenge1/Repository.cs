﻿using System.Globalization;

namespace Challenge1
{
    public struct Repository
	{
		private Worker[] workers;

		private string path;

		private int index;

		public Repository(string Path)
		{
			this.path = Path;
			this.index = 0;
			this.workers = new Worker[2];
			this.LoadFromFile();

        }

		private void Resize(bool flag)
		{
			Array.Resize(ref this.workers, this.workers.Length * 2);
		}

		private void Add(Worker concreteWorker)
		{
			this.Resize(index >= this.workers.Length);
			this.workers[index] = concreteWorker;
			this.index++;
		}

		private void LoadFromFile()
		{
			using (StreamReader sr = new StreamReader(this.path))
			{
				while (!sr.EndOfStream)
				{
					string[] args = sr.ReadLine()!.Split("#");
					int id = int.Parse(args[0]);
					DateTime addedTime = DateTime.ParseExact(args[1], "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string fullName = args[2];
					int age = int.Parse(args[3]);
					float height = float.Parse(args[4]);
					DateTime dateBirth = DateTime.ParseExact(args[5], "dd.MM.yyyy", CultureInfo.InvariantCulture);
                    string territory = args[6];
                    Add(new Worker(id, addedTime, fullName, age, height, dateBirth, territory));
				}
			}
		}

		private void SetToFile()
		{
			using (StreamWriter sw = File.CreateText(this.path))
			{
				for (int i = 0; i < this.index; i++)
				{
					sw.WriteLine(this.workers[i].WorkerInfoStringForFile());
				}
			}
		}

		private void RefreshFile()
		{
            this.SetToFile();
            this.index = 0;
            this.LoadFromFile();
        }

		public Worker[] GetAllWorkers()
		{
			return this.workers;
		}

		public Worker GetWorkerById(int id)
		{
			if (id < index)
			{
				return this.workers[id];
			}
			else
			{
				Console.WriteLine("Несуществующий id, получены данные последнего сотрудника");
				return this.workers[index - 1];
			}
		}

		public void DeleteWorker(int id)
		{
			if(id < index)
			{
                for (int i = id; i < index; i++)
                {
                    this.workers[i] = this.workers[i + 1];
                }
                index--;

                this.RefreshFile();
            }
			else
			{
				Console.WriteLine("Несуществующий id");
			}
		}

		public void AddWorker(string fullName, int age, float height, DateTime dateBirth, string territory)
		{
            DateTime addedDateTime = DateTime.Now;
            Add(new Worker(index, addedDateTime, fullName, age, height, dateBirth, territory));

			this.RefreshFile();
        }

		public void PrintAllWorkers()
		{
			for (int i = 0; i < this.index; i++)
			{
				Console.WriteLine(this.workers[i].WorkerInfoString());
			}
		}

		public void PrintWorkers(Worker[] workersForPrint)
		{
            for (int i = 0; i < workersForPrint.Length; i++)
            {
				Console.WriteLine(workersForPrint[i].WorkerInfoString());
            }
        }

        public void PrintWorkers(Worker workerForPrint)
        {
            Console.WriteLine(workerForPrint.WorkerInfoString());
        }

        public Worker[] GetWorkersBetweenTwoDates(DateTime dateFrom, DateTime dateTo)
		{
			var betweenWorkers = this.workers.Where(w => w.AddedTime >= dateFrom && w.AddedTime <= dateTo);

			Worker[] newWorker = betweenWorkers.ToArray();

            return newWorker;
		}
	}
}

