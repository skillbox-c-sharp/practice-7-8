﻿using System;
namespace Challenge1
{
	public struct Worker
	{
		public int ID { get; set; }
        public DateTime AddedTime { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
        public float Height { get; set; }
        public DateTime DateBirth { get; set; }
        public string Territory { get; set; }

        public Worker(int ID, DateTime AddedTime, string FullName, int Age, float Height, DateTime DateBirth, string Territory)
        {
            this.ID = ID;
            this.AddedTime = AddedTime;
            this.FullName = FullName;
            this.Age = Age;
            this.Height = Height;
            this.DateBirth = DateBirth;
            this.Territory = Territory;
        }

        public string WorkerInfoString()
        {
            string s1 = this.ID.ToString();
            string s2 = this.AddedTime.ToString("dd.MM.yyyy HH:mm:ss");
            string s3 = this.FullName;
            string s4 = this.Age.ToString();
            string s5 = this.Height.ToString();
            string s6 = this.DateBirth.ToString("dd.MM.yyyy");
            string s7 = this.Territory;

            return $"{s1} {s2} {s3} {s4} {s5} {s6} {s7}";
        }

        public string WorkerInfoStringForFile()
        {
            string s1 = this.ID.ToString();
            string s2 = this.AddedTime.ToString("dd.MM.yyyy HH:mm:ss");
            string s3 = this.FullName;
            string s4 = this.Age.ToString();
            string s5 = this.Height.ToString();
            string s6 = this.DateBirth.ToString("dd.MM.yyyy");
            string s7 = this.Territory;

            return $"{s1}#{s2}#{s3}#{s4}#{s5}#{s6}#{s7}";
        }
    }
}

